package ru.ilsurih.elasticsearch.task1;

import java.nio.ByteBuffer;
import java.util.*;

import static java.lang.Math.log;

public class Compressor {
    private static byte[] encodeNumber(int n) {
        if (n == 0) {
            return new byte[]{0};
        }
        int i = (int) (log(n) / log(128)) + 1;
        byte[] rv = new byte[i];
        int j = i - 1;
        do {
            rv[j--] = (byte) (n % 128);
            n /= 128;
        } while (j >= 0);
        rv[i - 1] += 128;
        return rv;
    }

    private static byte[] encode(Iterable<Integer> numbers, int size) {
        ByteBuffer buf = ByteBuffer.allocate(size * (Integer.SIZE / Byte.SIZE));
        for (Integer number : numbers) {
            buf.put(encodeNumber(number));
        }
        buf.flip();
        byte[] rv = new byte[buf.limit()];
        buf.get(rv);
        return rv;
    }

    private static TreeSet<Integer> decode(byte[] byteStream) {
        TreeSet<Integer> numbers = new TreeSet<Integer>();
        int n = 0;
        for (byte b : byteStream) {
            if ((b & 0xff) < 128) {
                n = 128 * n + b;
            } else {
                int num = (128 * n + ((b - 128) & 0xff));
                numbers.add(num);
                n = 0;
            }
        }
        return numbers;
    }

    public static Map<String, byte[]> compress(Map<String, TreeSet<Integer>> input) {
        Map<String, byte[]> out = new HashMap<>(input.size());
        for (Map.Entry<String, TreeSet<Integer>> entry: input.entrySet()) {
            out.put(entry.getKey(), encode(entry.getValue(), entry.getValue().size()));
        }
        return out;
    }

    public static Map<String, TreeSet<Integer>> decompress(Map<String, byte[]> input) {
        Map<String, TreeSet<Integer>> out = new HashMap<>(input.size());
        for (Map.Entry<String, byte[]> entry: input.entrySet()) {
            out.put(entry.getKey(), decode(entry.getValue()));
        }
        return out;
    }
}
