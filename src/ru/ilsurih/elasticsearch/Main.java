package ru.ilsurih.elasticsearch;

import org.apache.commons.io.IOUtils;
import ru.ilsurih.elasticsearch.task1.Compressor;
import ru.ilsurih.elasticsearch.task2.Document;
import ru.ilsurih.elasticsearch.task2.Score;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

public class Main {

    private static final String[] files = new String[]{
            "text1.txt",
            "text2.txt",
            "text3.txt",
            "text4.txt",
            "text5.txt",
            "text6.txt",
            "text7.txt",
            "text8.txt"
    };

    private static final String[] generated = new String[]{
            "file0.txt",
            "file1.txt",
            "file2.txt",
            "file3.txt",
            "file4.txt",
            "file5.txt",
            "file6.txt",
            "file7.txt",
            "file8.txt",
            "file9.txt",
    };

    public static void main(String[] args) throws Exception {
        if (args.length != 1 || !args[0].matches("[12]"))
            throw new Exception("Argument must be 1 or 2");
        if (args[0].equals("1")) {
            task1();
        } else task2();

    }

    private static void task1() throws IOException {
        Parser p = new Parser();
        for (int i = 0; i < files.length; i++) {
            InputStream inputStream = new FileInputStream(Main.class.getClassLoader().getResource(files[i]).getFile());
            String s = IOUtils.toString(inputStream);
            p.parse(s, i + 1);
        }
        printMap(p.getIndexes());
        System.out.println("\n\n\n\n\ncollection after compression and decompression");
        printMap(Compressor.decompress(Compressor.compress(p.getIndexes())));
    }

    private static void task2() throws IOException {
        List<Document> doc = new ArrayList<>(generated.length);
        for (int i = 0; i < generated.length; i++) {
            InputStream inputStream = new FileInputStream(Main.class.getClassLoader().getResource(generated[i]).getFile());
            String s = IOUtils.toString(inputStream);
            doc.add(new Document(generated[i], s));
        }
        Score sc = new Score(doc);

        String q;
        if (System.console() == null)
            q = "теннис соседка корректировка";
        else {
            System.out.println("Введите запрос");
            q = System.console().readLine();
        }
        doc.stream().forEach(d -> System.out.println(d.getName() + " has: " + sc.calc(d, q) + " score"));
    }

    private static void printMap(Map<String, TreeSet<Integer>> o) {
        List<Map.Entry<String, TreeSet<Integer>>> list = new ArrayList<>(o.entrySet());
        Collections.sort(list, (o2, o1) -> Integer.compare(o1.getValue().size(), o2.getValue().size()));
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, TreeSet<Integer>> tmp : list) {
            sb.append('"').append(tmp.getKey()).append("\":\t\t\t\t").append('{').append(tmp.getValue().stream().map(Object::toString).collect(Collectors.joining(","))).append("}\n");
        }
        System.out.print(sb.toString());
    }
}
