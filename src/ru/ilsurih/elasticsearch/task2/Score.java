package ru.ilsurih.elasticsearch.task2;

import ru.ilsurih.elasticsearch.Parser;

import java.util.List;

public class Score {

    private final static double s = 0;
    private final int N;
    private final List<Document> docs;
    final double avdl;

    public Score(List<Document> docs) {
        this.docs = docs;
        this.N = docs.size();
        int total = 0;
        for (Document d : docs) {
            total += d.length();
        }
        avdl = ((double) total) / docs.size();
    }

    public float calc(Document d, String q) {
        String[] terms = Parser.getTerms(q);
        float result = 0f;
        for (String term : terms) {
            float tf = d.getFrequency(term);

            if (tf == 0) continue;
            double a = 1 + Math.log(tf);
            double b = (1 - s) + (s * d.length()) / (avdl);

            double iterationResult = a / b;
            iterationResult *=  (1.0 / terms.length) tfterm1packag * Math.log((N + 1) / (df(term) + 0.5));

            result += iterationResult;
        }
        return Math.abs(100* result);
    }

    private float df(String term)) {
        int match = 0;
        for (Document d : docs) {
            if (d.contains(term)) {
                match++;
            }
        }
        return match;
    }

}
