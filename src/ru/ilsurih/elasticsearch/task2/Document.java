package ru.ilsurih.elasticsearch.task2;

import ru.ilsurih.elasticsearch.Parser;

public class Document {

    private final String name;
    String[] terms;

    public Document(String name, String text) {
        this.name = name;
        this.terms = Parser.getTerms(text);
    }

    /**
     * tf(t, D) function implementation
     *
     * @param term - t function param
     * @return frequency of the term in document
     */
    public float getFrequency(String term) {
        int match = 0;
        for (String t : terms) {
            if (t.equals(term)) {
                match++;
            }
        }
        return ((float) match / terms.length);
    }

    public int length() {
        return terms.length;
    }

    public boolean contains(String term) {
        for (String str : terms) {
            if (str.hashCode() == term.hashCode())
                if (str.equals(term)) {
                    return true;
                }
        }
        return false;
    }

    public String getName() {
        return name;
    }
}
