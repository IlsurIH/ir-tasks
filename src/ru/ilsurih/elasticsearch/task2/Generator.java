package ru.ilsurih.elasticsearch.task2;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Generator {

    final File input;
    private final String[] subjects = new String[]{
            "кошка",
            "корабль",
            "теннис",
            "растение",
            "биология",
            "автомобиль"
    };

//    public static void main (String [] o) {
//        new Generator().generate(10);
//    }

    public Generator() {
        input = new File(getClass().getClassLoader().getResource("db.txt").getFile());
    }

    public List<Document> generate(int count) {
        try {
            List<Document> documents = new ArrayList<>(count);
            List<String> words = getStrings(count);

            for (int i = 0; i < count; i++) {
                if (words.size() == 0) words = getStrings(count);
                String subject = subjects[random(subjects.length)];
                StringBuilder text = new StringBuilder();
                for (int a = 0; a < 300; a++) {
                    if (guess(random(10) + 5)) // 5-15% of subject word in text
                        text.append(subject);
                    else text.append(words.remove(random(words.size())));
                    if (guess(80))
                        text.append(" ");
                    else if (guess(80))
                        text.append(", ");
                    else text.append(".\n");
                }
                String string = text.toString();
                File file = new File("gen/file" + i + ".txt");
                file.getParentFile().mkdirs();
                file.createNewFile();
                FileOutputStream stream = new FileOutputStream(file);
                stream.write(string.getBytes("utf-8"));
                stream.close();
                Document d = new Document(file.getName(), string);
                documents.add(d);
            }
            return documents;
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
            return null;
        }
    }

    private ArrayList<String> getStrings(int count) throws IOException {
        int limit = Math.min(count * 300, 93392);
        ArrayList<String> strings = new ArrayList<>(300 * count);
        Scanner io = new Scanner(new FileInputStream(input));
        try {
            for (int i = 0; i < limit; i++) {
                strings.add(io.next());
            }
            return strings;
        } finally {
            io.close();
        }
    }

    private int random(int limit) {
        return (int) ((Math.random() * 10000) % limit);
    }

    /**
     * p% returns true
     *
     * @param percent - %
     * @return true or false
     */
    public boolean guess(int percent) {
        return (Math.random() * 100) <= percent;
    }

}
