package ru.ilsurih.elasticsearch;

import java.util.*;
import java.util.stream.Collectors;

public class Parser {

    private final HashMap<String, TreeSet<Integer>> indexes;

    public Parser() {
        indexes = new HashMap<String, TreeSet<Integer>>() {
            @Override
            public String toString() {
                StringBuilder sb = new StringBuilder();
                for (Entry<String, TreeSet<Integer>> tmp : entrySet()) {
                    sb.append('"').append(tmp.getKey()).append("\":\t\t\t\t").append('{').append(tmp.getValue().stream().map(Object::toString).collect(Collectors.joining(","))).append("}\n");
                }
                return sb.toString();
            }
        };
    }

    public void parse(String text, int index) {
        String[] split = getTerms(text);
        for (String word : split) {
            TreeSet<Integer> set = getSet(word);
            set.add(index);
        }
    }

    public static String[] getTerms(String text) {
        return text.replaceAll("[^а-яА-Я ]", "").toLowerCase().split("\\s+");
    }

    private TreeSet<Integer> getSet(String word) {
        TreeSet<Integer> res = indexes.get(word);
        if (res == null) {
            res = new TreeSet<>();
            indexes.put(word, res);
        }
        return res;
    }

    public HashMap<String, TreeSet<Integer>> getIndexes() {
        return indexes;
    }
}
